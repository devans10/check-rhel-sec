# check-rhel-sec

## Descritpion
The check-sec.py is meant to process a list of installed packages generated
from a server that does not have access to the internet.  It will verify that 
the packages are up-to-date against the [Red Hat Security Data API](https://access.redhat.com/labsinfo/securitydataapi). 

The list is generated using the get-system-package-info.py.  A group of files
can be processed together from within the same directory.

The script creates a local cache of the cvrf, and attempts to index them by
package name.  It compares the released_package and installed package RPM versions.
If the installed version is less then the released_package, then it reports that
a updated package is available.

## Setup

```bash
git clone https://gitlab.com/devans10/check-rhel-sec.git
cd check-rhel-sec
docker build --tag check-rhel-sec .
docker run -it --rm -v /path/to/datafiles:/data check-rhel-sec
```
