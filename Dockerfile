FROM centos:7

RUN yum update -y \
    && yum install -y python-requests

WORKDIR /usr/src/app

COPY . /usr/src/app

ENTRYPOINT ["python", "check-sec.py"]
CMD ["-f", "/data/*"]
