#!/usr/bin/python3.6
from __future__ import print_function

## Purpose: This script is designed to check a list of installed
##          RPM packages against the Red Hat Security API
##          to validate that no patches are applicable to
##          those packages.
##
## Owner: Linux Team
## Source: http://gitlab.dqe.com/linuxteam/cip-check-sec
## Version history:
## 8/17/2018 - Dave Evans - Initial Commit

import os
import sys
import rpm
import json
import glob
import requests
import logging
from datetime import datetime, timedelta, date
from optparse import OptionParser


def splitFilename(filename):
      """
      Pass in a standard style rpm fullname

      Return a name, version, release, epoch, arch, e.g.::
          bar-1:9-123a.ia64.rpm returns bar, 9, 123a, 1, ia64
      """

      if filename[-4:] == '.rpm':
          filename = filename[:-4]

      archIndex = filename.rfind('.')
      arch = filename[archIndex+1:]
      filename = filename[:archIndex]

      relIndex = filename.rfind('-')
      rel = filename[relIndex+1:]
      filename = filename[:relIndex]

      verIndex = filename.rfind(':')
      if verIndex == -1:
          epoch = '0'
          verIndex = filename.rfind('-')
          ver = filename[verIndex+1:]
          filename = filename[:verIndex]
          name = filename
      else:
          ver = filename[verIndex+1:]
          epochIndex = filename.rfind('-')
          epoch = filename[epochIndex+1]

          name = filename[:epochIndex]

      return name, ver, rel, epoch, arch

## Function to query RedHat Security API
def get_data(query,logger):

    full_query = query
    r = requests.get(full_query)
    
    retry = 1
    while r.status_code == 504 and rerty < 3:
        time.sleep(5)
        r = requests.get(full_query)
        r = r + 1

    if r.status_code != 200:
        logger.error('Invalid request; returned {} for the following '
              'query:\n{}'.format(r.status_code, full_query))
        raise Exception('Invalid request')
    if not r.json():
        logger.debug('No Data Returned')
        raise Exception('No Data Returned')

    return r.json()

def index_erratum_by_package(erratum, logger):

    logger.debug("Indexing Erratum")
    indexed_erratum = []
    for errata in erratum:
        for released_package in errata['released_packages']:
            package_errata = {"name": released_package, "RHSA":errata['RHSA'],"released_on":errata['released_on'], "resource_url":errata['resource_url']}
            indexed_erratum.append(package_errata)
    return indexed_erratum

def main():

    ## Parse the commandline options
    parser = OptionParser(usage="usage: %prog [options]", version="%prog 1.0")

    parser.add_option("-f", "--files", dest="files", action="store", help="files to be processed")
    parser.add_option("-o", "--output", dest="output", default="/data", help="path to output file")
    (options, args) = parser.parse_args(sys.argv)

    # Setup logging
    logFormatter = logging.Formatter("%(asctime)s [%(threadName)-22.22s] [%(levelname)-8.8s]  %(message)s")
    logger = logging.getLogger()
    logger.setLevel(os.environ.get("LOGLEVEL", "WARNING"))

    fileHandler = logging.FileHandler("{0}/{1}.log".format(options.output, "create-workbook-mt"))
    fileHandler.setFormatter(logFormatter)
    logger.addHandler(fileHandler)

    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    logger.addHandler(consoleHandler)

    # RedHat Security Data API
    api_url = "https://access.redhat.com/labs/securitydataapi"
    
    # Get date from 60 days ago
    date = datetime.now() - timedelta(days=60)


    # Create a list to hold local cache of package errata retrieved from Red Hat
    package_erratum = {}
    page = 1
    erratum = get_data(api_url+"/cvrf.json?page="+str(page),logger)
    while len(erratum) > 0:
        for errata in erratum:
            for package in errata['released_packages']:
                name, ver, rel, epoch, arch = splitFilename(package+'.arch.rpm')
                try:
                    package_erratum[name].append({"name": package,"RHSA": errata['RHSA'],"released_on": errata['released_on'],"resource_url": errata['resource_url']})
                except KeyError:
                    package_erratum[name] = []
                    package_erratum[name].append({ "name": package,"RHSA": errata['RHSA'],"released_on": errata['released_on'],"resource_url": errata['resource_url']})
        page = page + 1
        try:
            erratum = get_data(api_url+"/cvrf.json?page="+str(page),logger)
        except:
            erratum = []
    

    ## Get command line options and setup execution
    if options.files:
        file_list = glob.glob(options.files)
    else:
        logger.error("A file list or directory must be provided.")
        sys.exit(1)


    # Loop through files in the list
    for files in file_list:
        logger.warning('Processing file: %s' % (files))
        try:
            with open(files) as f:
                system_json = json.load(f)
        except:
            logger.error("Could not parse file: %s" % (files))
            continue
        

        ## Get the system info
        system_info = system_json['system']
        
        # format Procduct name like Red Hat lists it
        dist_major_ver = system_info['version'].split('.')[0]
        installed_product = "%s (v. %s)" % (system_info['distname'], dist_major_ver)

        # Get installed packages
        logger.warning('System: %s' % (system_info['hostname']))
        pkgs = system_info['installed_pkgs']

        # Loop through all install packages
        for pkg in pkgs:

            # set the package NVRA
            pkg_nvra = pkg['name']+"-"+pkg['version']+"-"+pkg['release']+"."+pkg['arch']

            # Check the local cache for the package
            try:
                package_errata = package_erratum[pkg['name']]
                logger.debug("cache hit for: %s" % pkg['name'])
            except KeyError:
                logger.debug("cache miss for: %s" % pkg['name'])
                # Query RedHat Security API for package errata
                try:
                    erratum = get_data(api_url+"/cvrf.json?package="+pkg['name'],logger)
                except Exception:
                    # No Data returned go to next package
                    package_erratum[pkg['name']] = []
                    continue
                
            # We got data back, index it into the local cache
            package_erratum[pkg['name']] = index_erratum_by_package(erratum, logger)
            package_errata = package_erratum[pkg['name']]
                
            if len(package_errata) == 0:
                logger.debug("No Errata to check")
                continue

            for package in package_errata:
                 (n, v, r, e, a) = splitFilename(package['name']+".arch.rpm")
                 #print("%s %s %s %s %s" % (n, v, r, e, a))
                 logger.debug("comparing %s to %s" % (n, pkg['name']))
                 if n == pkg['name'] and r.split('.')[1] == pkg['release'].split('.')[1]:
                     logger.warning("checking package: %s" % (pkg['name']))
                     if rpm.labelCompare((pkg['epoch'], pkg['version'], pkg['release']),(e,v,r)) < 0:
                         logger.warning("%s released on %s is not applied: %s" % 
                                    (package['RHSA'], package['released_on'], package['name']))








if __name__ == "__main__":
    main()
