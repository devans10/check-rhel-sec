#!/usr/bin/python

## Purpose: This script is designed to get the last yum 
##          transaction and output all updated and 
##          installed packages to a file.
##          
## Owner: Linux Team
## Source: http://gitlab.dqe.com/linuxteam/satellite-promote
## Version history:
## 6/20/2018 - Dave Evans - Initial Commit

## Import python modules
import json
import socket
import time
import yum
import platform

# Get system information
sysinfo = {}
sysinfo['hostname'] = socket.gethostname()
sysinfo['distname'], sysinfo['version'], sysinfo['id'] = platform.linux_distribution()

# initialize a yum session for the server so 
# so it can be read by non-root users.
yb = yum.YumBase()
yb.setCacheDir()

# Get installed RPMs
sysinfo['installed_pkgs'] = []
installed_pkgs = yb.rpmdb.returnPackages()
for pkg in installed_pkgs:
    sysinfo['installed_pkgs'].append({'name':pkg.name,'epoch':pkg.epoch, 'version':pkg.version, 'release':pkg.release, 'arch':pkg.arch})

# get the last yum transaction
last_transaction = yb.history.last()

applied_pkgs = []
for pkg in last_transaction.trans_data:
    if pkg['state'] in ['Update', 'Dep-Install', 'Install', 'True-Install']:
        applied_pkgs.append({'name':pkg.name,'epoch':pkg.epoch, 'version':pkg.version, 'release':pkg.release, 'arch':pkg.arch})
        #if pkg['epoch'] == '0':
        #    applied_pkgs.append(pkg['name']+'-'+pkg['version']+'-'+pkg['release'])
        #else:
        #    applied_pkgs.append(pkg['name']+'-'+pkg['epoch']+':'+pkg['version']+'-'+pkg['release'])

output_json = { 'system': sysinfo, 'last_transaction': applied_pkgs }
# Print the output
workbook_filename = "/tmp/"+socket.gethostname()+"_last_trans_" + time.strftime("%Y%m%d") + ".out"
with open(workbook_filename, 'w') as outfile:
    json.dump(output_json, outfile)
